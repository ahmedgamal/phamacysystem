<?php

namespace DoctrineORMModule\Proxy\__CG__\Richstore\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class InvoiceItems extends \Richstore\Entity\InvoiceItems implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array('batchNo' => NULL, 'unitCost' => NULL, 'totalCost' => NULL, 'amount' => NULL, 'bonus' => NULL, 'sellingPrice' => NULL, 'discount1' => NULL, 'discount2' => NULL, 'totalAmount' => NULL);



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {
        unset($this->batchNo, $this->unitCost, $this->totalCost, $this->amount, $this->bonus, $this->sellingPrice, $this->discount1, $this->discount2, $this->totalAmount);

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * 
     * @param string $name
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->__getLazyProperties())) {
            $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', array($name));

            return $this->$name;
        }

        trigger_error(sprintf('Undefined property: %s::$%s', __CLASS__, $name), E_USER_NOTICE);
    }

    /**
     * 
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->__getLazyProperties())) {
            $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', array($name, $value));

            $this->$name = $value;

            return;
        }

        $this->$name = $value;
    }

    /**
     * 
     * @param  string $name
     * @return boolean
     */
    public function __isset($name)
    {
        if (array_key_exists($name, $this->__getLazyProperties())) {
            $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', array($name));

            return isset($this->$name);
        }

        return false;
    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'product', 'expieryDate', 'purchasePrice', 'batchNo', 'unitCost', 'totalCost', 'amount', 'bonus', 'sellingPrice', 'discount1', 'discount2', 'totalAmount', 'invoice');
        }

        return array('__isInitialized__', 'id', 'product', 'expieryDate', 'purchasePrice', 'invoice');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (InvoiceItems $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

            unset($this->batchNo, $this->unitCost, $this->totalCost, $this->amount, $this->bonus, $this->sellingPrice, $this->discount1, $this->discount2, $this->totalAmount);
        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setProduct($product)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProduct', array($product));

        return parent::setProduct($product);
    }

    /**
     * {@inheritDoc}
     */
    public function getProduct()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProduct', array());

        return parent::getProduct();
    }

    /**
     * {@inheritDoc}
     */
    public function setExpieryDate($expieryDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExpieryDate', array($expieryDate));

        return parent::setExpieryDate($expieryDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getExpieryDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExpieryDate', array());

        return parent::getExpieryDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setPurchasePrice($purchasePrice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPurchasePrice', array($purchasePrice));

        return parent::setPurchasePrice($purchasePrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getPurchasePrice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPurchasePrice', array());

        return parent::getPurchasePrice();
    }

    /**
     * {@inheritDoc}
     */
    public function setBatchNo($batchNo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBatchNo', array($batchNo));

        return parent::setBatchNo($batchNo);
    }

    /**
     * {@inheritDoc}
     */
    public function getBatchNo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBatchNo', array());

        return parent::getBatchNo();
    }

    /**
     * {@inheritDoc}
     */
    public function setInvoice(\Richstore\Entity\InvoiceHeader $invoice = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setInvoice', array($invoice));

        return parent::setInvoice($invoice);
    }

    /**
     * {@inheritDoc}
     */
    public function getInvoice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getInvoice', array());

        return parent::getInvoice();
    }

}
