(function() {
    var as = angular.module('richstore.controllers', ['ui.keypress','richstore.api','ngGrid','ui.select2','ui.bootstrap']);
    as.controller('AppCtrl', function($scope, $rootScope, $http, i18n, $location) {
        $scope.language = function() {
            return i18n.language;
        };
        $scope.setLanguage = function(lang) {
            i18n.setLanguage(lang);
        };
        $scope.activeWhen = function(value) {
            return value ? 'active' : '';
        };
        $scope.i18n = function(key) {
            return i18n.prop(key);
        };
       
        $scope.path = function() {
            return $location.url();
        };
        
        $scope.login = function() {
            $scope.$emit('event:loginRequest', $scope.username, $scope.password);
            //$location.path('/login');
        };

        $scope.logout = function() {
            $rootScope.user = null;
            $rootScope.username = $scope.password = null;
            $rootScope.$emit('event:logoutRequest');
            $location.url('/');
        };
       // console.log($scope.currentUser.userId);
        

        $rootScope.alerts = [];  
        $rootScope.stores =  [ { name: 'centrino', id: 1 },        { name: 'centrino2', id: 2 }];
        $rootScope.currentStore = $scope.stores[0]['id']; 
    	$rootScope.pageTitle="";

    	$scope.baseUrl= angular.element("#baseUrl").val();
    	$scope.currentUser={};
    	$scope.currentUser.userId= angular.element("#currentUserId").val();
    	$scope.currentUser.userRoleId= angular.element("#currentUserRole").val();
        if($scope.currentUser.userId==null){

              location.href=($scope.baseUrl+'/login');
             

          }

    });
    
    as.controller('NewPurchaseCtrl',['$scope', '$rootScope', '$http', '$location','api','crud', function($scope, $rootScope, $http, $location,api,crud) {
    	$rootScope.pageTitle="NewPurchase";

        $scope.suppliers =  [ { name: 'vendor1', id: 1 },        { name: 'vendor2', id: 2 }];

    	$scope.p={};
    	$scope.p.purchaseItems =[];
    	$scope.p.supplierId = $scope.suppliers[0]['id'];
    	
    	$scope.$watch('p.purchaseItems', function(newData,oldData ){
    		$scope.p.totalPrice = 0;
    		angular.forEach($scope.p.purchaseItems,function(item,idx){
    			
    			item.totalCost = (item["amount"] *item["defaultCustomerPrice"]* (1-((item["discount"]+item["discount2"])/100)));
    			$scope.p.totalPrice += item.totalCost ;
    			item.totalAmount = item["amount"] +item["bonus"];
 
    			
    			item.unitCost =  (item["amount"] * item["defaultCustomerPrice"]* (1-((item["discount"]+item["discount2"])/100))) / (item["amount"] + item["bonus"]) ;
    			
    		});
       	},true);

    	$scope.gridOptions ={
    			data:'p.purchaseItems',
    			enableCellEdit: true,
    			enableCellSelection: true,
                resizable: true,
    	        tabIndex: 1,
    	        columnDefs: [{field:'name', displayName:'Name', width:100, enableCellEdit: false,},
    	                     {field:'batchNo', displayName:'batchNo  ', width:100, enableCellEdit: true,},
    	                     {field:'expiryDate', displayName:'expiryDate  ',width:100, enableCellEdit: true, editableCellTemplate: '<input  type="date" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'amount', displayName:'amount  ',width:70,enableCellEdit: true,editableCellTemplate: '<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'bonus', displayName:'bonus  ',width:70,enableCellEdit: true,editableCellTemplate: '<input  type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'totalAmount', displayName:'totalAmount  ', width:100,editableCellTemplate: '<div>{{row.entity["amount"] + row.entity["bonus"]  }}</div>'},
    	                     {field:'defaultCustomerPrice', displayName:'Customer Price  ',width:100,enableCellEdit: true,editableCellTemplate: '<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"   ng-model="COL_FIELD">'},   	                     
    	                     {field:'discount', displayName:'discount  ',width:100,enableCellEdit: true,editableCellTemplate: '<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD">'},
    	                     {field:'discount2', displayName:'discount2  ',width:100,enableCellEdit: true,editableCellTemplate:'<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'totalCost', displayName:'totalCost  ', width:100 },
    	                     {field:'unitCost', displayName:'unitlCost  ', width:100},
    	                     

    	                      
    	        		]

    			}; 
    	$scope.suggestedProducts =[]; 
    	
  		$scope.productSelectOptions  =	{
    		    placeholder: "Search for a Product",
    		    minimumInputLength: 1,
    		    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
    		        url:  $scope.baseUrl+"/api/product/search",
    		        dataType: 'json',
    		        type : 'POST',
    		        data: function (term, page) {
    		            return {
    		            	searchText: term, // search term
    		                page_limit: 10//,
    		            };
    		        },
    		        results: function (data, page) { // parse the results into the format expected by Select2.
    		        	// since we are using custom formatting functions we do not need to alter remote JSON data

    		        	
    		        	$scope.suggestedProducts = data;
    		        	
    		        	
    		            return {results: data};
    		        }
    		    },
    		    initSelection:  function (m) {
    		    	$scope.selectedProduct = m;
        			$scope.p.purchaseItems.push(m);	
    		    	return m; 
    		    	},
    		    formatResult:  function (m) { 	return   m.name ; }, // omitted for brevity, see the source of this page
    		    formatSelection:  function (m) { return m; },  // omitted for brevity, see the source of this page
    		    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    		    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    		};
    		

    	$scope.addProduct = function (event){
    		$scope.selectedProduct.discount = 0 ;
    		$scope.selectedProduct.discount2 =0 ;
    		$scope.p.purchaseItems.push($scope.selectedProduct);	
    		$scope.selectedProduct="";
    		
    	};
    	$scope.addOrder = function (event){
        	$scope.p.storeId= $scope.currentStore;
     		 x = crud.save({'resource':'purchase'},$scope.p,function(rs){
    			        	$scope.alerts.push(rs);
                            $location.path('#/order/list');
    			        });
    		$scope.productName="";
    	};

    }]);

    as.controller('ListInventoryCtrl',['$scope', '$rootScope', '$http', '$location','$filter','crud','i18n',function($scope, $rootScope, $http, $location, $filter,crud,i18n) {
    	$rootScope.pageTitle="inventoryContentss";
    	$scope.filterParams={};
        $scope.gridOptions = { 
        			data: 'inventoryItems',
                    resizable: true,
        	        columnDefs: [
        	                     {field:'id', displayName:'id', width:100, enableCellEdit: false,},
        	                     {field:'product.name', displayName:'Name', width:100, enableCellEdit: false,},
        	                     {field:'invoiceItem.batchNo', displayName:'batchNo  ', width:100, },
        	                     {field:'expiryDate.date', displayName:'expiryDate  ',width:100,},
        	                     {field:'balance', displayName:'Balance',width:100,},
        	                     {field:'invoiceItem.unitCost', displayName:'unitlCost  ', width:100},
        	                     {field:'invoiceItem.invoice.supplier.name', displayName:'supplierName  ', width:100},
        	                     {field:'store.name', displayName:'Store  ', width:100},
        	                      
        	        		]
        		};
        //$scope.suppliers  = crud.query({'resource':'party',role:1});
        $scope.suppliers  = [{id:1,name:'nagdy'},{id:2,name:'alpha'},];
        $scope.inventoryItems  = crud.query({'resource':'inventory',storeId:$scope.currentStore});
        $scope.search = function(filterParams){
        	filterParams['resource'] = 'inventory';
        	filterParams.expiryDatebefore= $filter('date')(filterParams.expiryDatebefore, 'yyyy-MM-dd');
        	$scope.inventoryItems  = crud.query(filterParams);
        };
        $scope.clear = function(){
        	$scope.filterParams={};
        	$scope.search({});
        };
    	


    }]);

    as.controller('ListOrdersCtrl',['$scope', '$rootScope', '$http', '$location','$filter','crud','i18n',function($scope, $rootScope, $http, $location, $filter,crud,i18n) {
    	$rootScope.pageTitle="Orders";
    	$scope.filterParams={};
        $scope.orders  = crud.query({'resource':'order'});
        $scope.gridOptions = { 
            data: 'orders',
            resizable: true,
            columnDefs: [
                         {field:'id', displayName:'id', width:100},
                         {field:'totalPrice', displayName:'Total Price ', width:150 },
                         {field:'finalPrice', displayName:'Final Price', width:150},
                         
                         {field:'dateCreated.date', displayName:'expiryDate  ',width:100,},
                         {field:'orderItems.length', displayName:'items Count',width:100,},
                         //{field:'"show details"', displayName:'',width:100,}
                         {field: 'id',  displayName: 'Link',  enableCellEdit: false,  cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><a href="#order/view/{{row.getProperty(col.field)}}">show order</a></div>'
}
                         
                          
                    ]
        };
        return ;


    }]);
    as.controller('ViewOrderCtrl',['$scope', '$rootScope', '$http', '$location','$filter','crud','i18n','$routeParams',function($scope, $rootScope, $http, $location, $filter,crud,i18n, $routeParams) {
    	$rootScope.pageTitle="Order:"+ $routeParams.id;
    	
        $scope.order  = crud.get({'resource':'order','id':$routeParams.id});
    	$scope.gridOptions ={
    			data:'order.orderItems',
    			enableCellEdit: true,
    			enableCellSelection: true,
    	        columnDefs: [{field:'product.name', displayName:'Name', width:100, enableCellEdit: false,},
    	                     {field:'amount', displayName:'amount  ',width:100,enableCellEdit: true,editableCellTemplate: '<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'totalItemPrice', displayName:'totalCost  ', width:100, enableCellEdit: false },
    	        		]
    			}; 
        return ;


    }]);
    
    
    as.controller('NewOrderCtrl',['$scope', '$rootScope', '$http', '$location','api','crud', function($scope, $rootScope, $http, $location,api,crud) {
    	$rootScope.pageTitle="NewSale";

    	
    	$scope.customers =  [ { name: 'Customer1', id: 1 },        { name: 'Customer2', id: 2 }];

    	$scope.order={};
    	$scope.order.orderItems =[];
    	$scope.order.customerId = 0;
    	$scope.order.discount = 0;
    	
 
    	$scope.$watch('order', function(newData,oldData ){
    	    		$scope.order.totalPrice = 0;
    		angular.forEach($scope.order.orderItems,function(item,idx){
    			item.totalItemPrice = item["amount"] * item["invoiceItem"]["sellingPrice"];
    			$scope.order.totalPrice += item.totalItemPrice ;
    			$scope.order.finalPrice=(1-($scope.order.discount/100))*$scope.order.totalPrice
   			
    			
    		});
       	},true);

    	$scope.gridOptions ={
    			data:'order.orderItems',
    			enableCellEdit: true,
    			enableCellSelection: true,
    	        columnDefs: [{field:'product.name', displayName:'Name', width:100, enableCellEdit: false,},
    	                     {field:'expiryDate.date', displayName:'expiryDate  ',width:200,},
    	                     {field:'amount', displayName:'amount  ',width:100,enableCellEdit: true,editableCellTemplate: '<input type="number"  ng-class="\'colt\' + col.index"  ng-input="COL_FIELD"  ng-model="COL_FIELD">'},
    	                     {field:'invoiceItem.sellingPrice', displayName:'Customer Price  ',width:150, enableCellEdit: false},   	                     
    	                     {field:'invoiceItem.unitCost', displayName:'unitCost  ',width:150, enableCellEdit: false},
    	                     {field:'totalItemPrice', displayName:'totalCost  ', width:150, enableCellEdit: false },
    	        		]
    			}; 
    	$scope.suggestedProducts =[]; 
    	
  		$scope.productSelectOptions  =	{
    		    placeholder: "Search for a Product",
    		    minimumInputLength: 1,
    		    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
    		        url:  $scope.baseUrl+"/crud/inventory",
    		        dataType: 'json',
    		        type : 'GET',
    		        data: function (term, page) {
    		            return {
    		            	searchText: term, // search term
    		                page_limit: 10//,
    		            };
    		        },
    		        results: function (data, page) { // parse the results into the format expected by Select2.
    		        	// since we are using custom formatting functions we do not need to alter remote JSON data
    		            return {results: data};
    		        }
    		    },
    		    initSelection:  function (m) {
    		    	$scope.selectedProduct = m;
        			$scope.order.orderItems.push(m);	
    		    	return m; 
    		    	},
    		    formatResult:  function (m) {
    		    	return    m.product.name+"("+m.expiryDate.date+")-("+m.balance+")"  ; }, // omitted for brevity, see the source of this page
    		    formatSelection:  function (m) { return m.name; },  // omitted for brevity, see the source of this page
    		    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    		    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    		};
    		

    	$scope.addProduct = function (event){
    		$scope.selectedProduct.amount=1;
    		$scope.order.orderItems.push($scope.selectedProduct);	
    		$scope.selectedProduct="";
    		
    	};
    	$scope.addOrder = function (event){
        	$scope.order.storeId= $scope.currentStore;
     		 x = crud.save({'resource':'order'},$scope.order,function(rs){
    			        	$rootScope.alerts.push(rs);
                            $location.path('/order/list');
    			        });
    		$scope.productName="";
    	};
    	
    	 
    	
    }]);
    

    as.controller('LowstockInventoryCtrl',['$scope', '$rootScope', '$http', '$location','$filter','crud','i18n','api',function($scope, $rootScope, $http, $location, $filter,crud,i18n,api) {
    	$rootScope.pageTitle="Low Stock";
    	$scope.filterParams={};
        $scope.inventoryItems  = api.list({'resource':'inventory','action':'lowStock'},{});
        $scope.gridOptions = { 
        			data: 'inventoryItems',
                    resizable: true,
        	        columnDefs: [
        	                     {field:'id', displayName:'id', width:100, enableCellEdit: false,},
        	                     {field:'name', displayName:'Name', width:150, enableCellEdit: false,},
        	                     {field:'stock', displayName:'current stock  ', width:150, },
        	                     {field:'minimumStockAmt', displayName:'minimumStockAmt  ',width:150,} 
        	                      
        	        		]
        		};
        //$scope.suppliers  = crud.query({'resource':'party',role:1});
    	


    }]);
}());

