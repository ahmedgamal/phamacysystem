'use strict';

/* Filters */

angular.module('richstore.filters', []).
        filter('interpolate', ['version', function(version) {
                return function(text) {
                    return String(text).replace(/\%VERSION\%/mg, version);
                }
            }]);
