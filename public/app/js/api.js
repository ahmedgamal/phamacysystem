		 // Tell Angular to load the ngResource before loading our
    // custom app module.
    var apis = angular.module( "richstore.api", [ "ngResource" ] );
    apis.factory('crud', ['$resource','$rootScope',
                         function($resource,$rootScope){
                           return $resource('crud/:resource/:id', {start:0,limit:100});
                         }]);
      
    apis.factory('api', ['$resource','$rootScope',
                         function($resource,$rootScope){
                           return $resource('api/:resource/:action', {}, {
                        	   	'execute': {method:'POST',params:{}},
                      	 		'list': {method:'POST',params:{},  isArray:true}
                                 
                           });
                         }]);
      
      /*
    // Run this when the app is ready.
    app.run(
        function( $resource ) {


            // When defining the resource, we get a few actions
            // out of the box, like get() and query(), based on
            // standard HTTP verbs. But, we can also use RESTful
            // controller to change resource state using an
            // action that falls outside normal CRUD operations.
            var messages = $resource(
                "./api.cfm/messages/:listController:id/:docController",
                {
                    id: "@id",
                    listController: "@listController",
                    docController: "@docController"
                },
                {
                    clear: {
                        method: "POST",
                        params: {
                            listController: "clear-all"
                        }
                    },
                    archive: {
                        method: "POST",
                        params: {
                            docController: "archive"
                        }
                    }
                }
            );

    
        }
    );

    */
