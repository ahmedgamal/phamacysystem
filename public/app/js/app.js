(function() {

    var
            //the HTTP headers to be used by all requests
            httpHeaders,
            //the message to be shown to the user
            message,
            as = angular.module('richstore', ['richstore.filters', 'richstore.services', 'richstore.directives', 'richstore.controllers', 'richstore.api' ]);

    as.value('version', '1.0.7');

    as.config(function($routeProvider, $httpProvider   ) {
    	 
        $routeProvider
		        .when('/purchase/new', {templateUrl: 'app/partials/purchase/new.html', controller: 'NewPurchaseCtrl'})
				.when('/purchase/list', {templateUrl: 'app/partials/purchase/list.html', controller: 'ListPurchasesCtrl'})
				.when('/purchase/view/:id', {templateUrl: 'app/partials/purchase/view.html', controller: 'viewPurchaseCtrl'})
				.when('/inventory/new', {templateUrl: 'app/partials/inventory/new.html', controller: 'NewInventoryCtrl'})
        		.when('/inventory/list', {templateUrl: 'app/partials/inventory/list.html', controller: 'ListInventoryCtrl'})
                .when('/inventory/lowstock', {templateUrl: 'app/partials/inventory/lowstock.html', controller: 'LowstockInventoryCtrl'})
                .when('/inventory/view/:id', {templateUrl: 'app/partials/inventory/view.html', controller: 'viewInventoryCtrl'})
        		.when('/order/new', {templateUrl: 'app/partials/order/new.html', controller: 'NewOrderCtrl'})
                .when('/order/list', {templateUrl: 'app/partials/order/list.html', controller: 'ListOrdersCtrl'})
                .when('/order/view/:id', {templateUrl: 'app/partials/order/view.html', controller: 'ViewOrderCtrl'})
                .otherwise({redirectTo: '/order/new'});
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
    });
    
    as.config(['$httpProvider', function ($httpProvider) {
        var $http,
            interceptor = ['$q', '$injector', function ($q, $injector) {
                var error;

                function success(response) {
                    // get $http via $injector because of circular dependency problem
                	//console.log('#success');
                	//console.log(response);
                    

                    $http = $http || $injector.get('$http');
                    if($http.pendingRequests.length < 1) {
                        //console.log('#error');
                    }
                    return response;
                }

                function error(response) {
                    // get $http via $injector because of circular dependency problem
                    $http = $http || $injector.get('$http');
                    if($http.pendingRequests.length < 1) {
                   //   console.log('#error');
                    }
                    return $q.reject(response);
                }

                return function (promise) {
                //	console.log('#promise');
                    return promise.then(success, error);
                }
            }];

        $httpProvider.responseInterceptors.push(interceptor);
    }]);
  
       

        }());
