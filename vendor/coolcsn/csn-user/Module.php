<?php
/**
 * This file is placed here for compatibility with ZendFramework 2's ModuleManager.
 * The original Module.php is in 'src/CsnUser' in order to respect PSR-0
 */
require_once __DIR__ . '/src/CsnUser/Module.php';
