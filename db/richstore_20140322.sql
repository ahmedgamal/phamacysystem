-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: richstore
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PO_header`
--

DROP TABLE IF EXISTS `PO_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PO_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_price` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('created','requested','canceled','received') COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `comments` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PO_header_user1_idx` (`created_by`),
  CONSTRAINT `fk_PO_header_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PO_header`
--

LOCK TABLES `PO_header` WRITE;
/*!40000 ALTER TABLE `PO_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `PO_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PO_item`
--

DROP TABLE IF EXISTS `PO_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PO_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PO_header_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PO_items_PO_header1` (`PO_header_id`),
  CONSTRAINT `fk_PO_items_PO_header1` FOREIGN KEY (`PO_header_id`) REFERENCES `PO_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PO_item`
--

LOCK TABLES `PO_item` WRITE;
/*!40000 ALTER TABLE `PO_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `PO_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_transaction`
--

DROP TABLE IF EXISTS `account_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_transaction` (
  `id` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_account_transaction_user1_idx` (`user_id`),
  CONSTRAINT `fk_account_transaction_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_transaction`
--

LOCK TABLES `account_transaction` WRITE;
/*!40000 ALTER TABLE `account_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_category_idx` (`parent_category_id`),
  CONSTRAINT `fk_category_category` FOREIGN KEY (`parent_category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_attr`
--

DROP TABLE IF EXISTS `control_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_control_id` int(11) DEFAULT NULL,
  `attr_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr_value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table1_inventory_control1_idx` (`inventory_control_id`),
  CONSTRAINT `fk_table1_inventory_control1` FOREIGN KEY (`inventory_control_id`) REFERENCES `inventory_control` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_attr`
--

LOCK TABLES `control_attr` WRITE;
/*!40000 ALTER TABLE `control_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `control_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_transaction`
--

DROP TABLE IF EXISTS `control_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_control_id` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_control_transaction_inventory_control1_idx` (`inventory_control_id`),
  KEY `fk_control_transaction_user1_idx` (`user_id`),
  CONSTRAINT `fk_control_transaction_inventory_control1` FOREIGN KEY (`inventory_control_id`) REFERENCES `inventory_control` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_control_transaction_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_transaction`
--

LOCK TABLES `control_transaction` WRITE;
/*!40000 ALTER TABLE `control_transaction` DISABLE KEYS */;
INSERT INTO `control_transaction` VALUES (1,9,2.00,'Purchase Invoice No52',NULL,2,'2014-03-19'),(2,10,5.00,'Purchase Invoice No52',NULL,2,'2014-03-19'),(3,11,2.00,'Purchase Invoice No57',NULL,2,'2014-03-19'),(4,12,2.00,'Purchase Invoice No58',NULL,2,'2014-03-19'),(5,13,2.00,'Purchase Invoice No59',NULL,2,'2014-03-19'),(6,14,2.00,'Purchase Invoice No60',NULL,2,'2014-03-19'),(7,15,2.00,'Purchase Invoice No61',NULL,2,'2014-03-19'),(8,16,2.00,'Purchase Invoice No62',NULL,2,'2014-03-19'),(9,17,2.00,'Purchase Invoice No63',NULL,2,'2014-03-19'),(10,18,2.00,'Purchase Invoice No64',NULL,2,'2014-03-19'),(11,19,2.00,'Purchase Invoice No65',NULL,2,'2014-03-19'),(12,20,2.00,'Purchase Invoice No66',NULL,2,'2014-03-19'),(13,21,2.00,'Purchase Invoice No68',NULL,2,'2014-03-19'),(14,22,2.00,'Purchase Invoice No69',NULL,2,'2014-03-19'),(15,23,4.00,'Purchase Invoice No71',NULL,2,'2014-03-19'),(16,24,6.00,'Purchase Invoice No71',NULL,2,'2014-03-19'),(17,25,5.00,'Purchase Invoice No72',NULL,2,'2014-03-21'),(18,26,2.00,'Purchase Invoice No72',NULL,2,'2014-03-21'),(19,27,2.00,'Purchase Invoice No72',NULL,2,'2014-03-21'),(20,28,2.00,'Purchase Invoice No72',NULL,2,'2014-03-21'),(21,29,23.00,'Purchase Invoice No72',NULL,2,'2014-03-21'),(22,30,2.00,'Purchase Invoice No74',NULL,2,'2014-03-21'),(23,31,2.00,'Purchase Invoice No75',NULL,2,'2014-03-21'),(24,32,2.00,'Purchase Invoice No77',NULL,2,'2014-03-21'),(25,33,2.00,'Purchase Invoice No78',NULL,2,'2014-03-22'),(26,34,4.00,'Purchase Invoice No80',NULL,2,'2014-03-22'),(27,35,2.00,'Purchase Invoice No81',NULL,2,'2014-03-22'),(28,31,-3.00,'Order Invoice No6',NULL,2,'2014-03-22'),(29,31,-4.00,'Order Invoice No6',NULL,2,'2014-03-22'),(30,31,-5.00,'Order Invoice No6',NULL,2,'2014-03-22'),(31,35,-1.00,'Order Invoice No7',NULL,2,'2014-03-22');
/*!40000 ALTER TABLE `control_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`friend_id`),
  KEY `IDX_21EE7069A76ED395` (`user_id`),
  KEY `IDX_21EE70696A5458E8` (`friend_id`),
  CONSTRAINT `FK_21EE70696A5458E8` FOREIGN KEY (`friend_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_21EE7069A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl_account`
--

DROP TABLE IF EXISTS `gl_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl_account` (
  `id` int(11) NOT NULL,
  `type` enum('asset','liability') COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_gl_account_id` int(11) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gl_account_gl_account1_idx` (`parent_gl_account_id`),
  CONSTRAINT `fk_gl_account_gl_account1` FOREIGN KEY (`parent_gl_account_id`) REFERENCES `gl_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl_account`
--

LOCK TABLES `gl_account` WRITE;
/*!40000 ALTER TABLE `gl_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_control`
--

DROP TABLE IF EXISTS `inventory_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_item_id` int(11) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `location` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inventory_item_invoice_items1_idx` (`invoice_item_id`),
  CONSTRAINT `inventory_control_ibfk_1` FOREIGN KEY (`invoice_item_id`) REFERENCES `invoice_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_control`
--

LOCK TABLES `inventory_control` WRITE;
/*!40000 ALTER TABLE `inventory_control` DISABLE KEYS */;
INSERT INTO `inventory_control` VALUES (9,28,2.00,'2009-09-09',NULL,NULL,2,0),(10,29,5.00,'2009-09-09',NULL,NULL,2,0),(11,30,2.00,'2014-03-19',NULL,NULL,1,0),(12,31,2.00,'2009-09-09',NULL,NULL,1,0),(13,32,2.00,'2009-09-09',NULL,NULL,1,0),(14,33,2.00,'2009-09-09',NULL,NULL,1,0),(15,34,2.00,'2009-09-09',NULL,NULL,1,0),(16,35,2.00,'9009-01-01',NULL,NULL,1,0),(17,36,2.00,'2001-01-01',NULL,NULL,1,0),(18,37,2.00,'2001-01-01',NULL,NULL,1,0),(19,38,2.00,'2001-01-01',NULL,NULL,1,0),(20,39,2.00,'2001-01-01',NULL,NULL,1,0),(21,40,2.00,'2001-01-01',NULL,NULL,1,0),(22,41,2.00,'2001-01-01',NULL,NULL,1,0),(23,42,4.00,'2010-10-10',NULL,NULL,1,0),(24,43,6.00,'2009-01-01',NULL,NULL,1,0),(25,44,5.00,'2010-10-10',NULL,NULL,1,0),(26,45,2.00,'2010-10-10',NULL,NULL,1,0),(27,46,2.00,'2010-10-10',NULL,NULL,1,0),(28,47,2.00,'2010-10-10',NULL,NULL,1,0),(29,48,23.00,'2010-10-10',NULL,NULL,1,0),(30,50,0.00,'2010-10-10',NULL,NULL,1,2),(31,51,-10.00,'2010-10-10',NULL,NULL,1,2),(32,53,2.00,'2010-10-10',NULL,NULL,1,2),(33,54,2.00,'2010-10-10',NULL,NULL,1,3),(34,55,4.00,'2010-10-10',NULL,NULL,1,6),(35,56,1.00,'2015-10-12',NULL,NULL,1,2);
/*!40000 ALTER TABLE `inventory_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_header`
--

DROP TABLE IF EXISTS `invoice_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_headercol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PO_header_id` int(11) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `payment_method` enum('cash','credit') COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `items_price` decimal(10,2) DEFAULT NULL,
  `tax_price` decimal(10,2) DEFAULT NULL,
  `status` enum('recived','stored') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Invoice_PO_header1_idx` (`PO_header_id`),
  KEY `fk_Invoice_user1_idx` (`created_by`),
  CONSTRAINT `fk_Invoice_PO_header1` FOREIGN KEY (`PO_header_id`) REFERENCES `PO_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Invoice_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_header`
--

LOCK TABLES `invoice_header` WRITE;
/*!40000 ALTER TABLE `invoice_header` DISABLE KEYS */;
INSERT INTO `invoice_header` VALUES (52,NULL,NULL,60.92,'2014-03-18 22:36:26',2,NULL,1,NULL,NULL,NULL),(53,NULL,NULL,NULL,'2014-03-18 22:41:43',2,NULL,1,NULL,NULL,NULL),(54,NULL,NULL,NULL,'2014-03-18 22:42:08',2,NULL,1,NULL,NULL,NULL),(55,NULL,NULL,0.00,'2014-03-18 22:44:05',2,NULL,1,NULL,NULL,NULL),(56,NULL,NULL,NULL,'2014-03-18 22:56:10',2,NULL,1,NULL,NULL,NULL),(57,NULL,NULL,1.96,'2014-03-18 22:57:11',2,NULL,1,NULL,NULL,NULL),(58,NULL,NULL,0.98,'2014-03-18 23:20:27',2,NULL,1,NULL,NULL,NULL),(59,NULL,NULL,0.98,'2014-03-19 00:32:14',2,NULL,1,NULL,NULL,NULL),(60,NULL,NULL,0.98,'2014-03-19 00:32:43',2,NULL,1,NULL,NULL,NULL),(61,NULL,NULL,0.98,'2014-03-19 00:33:14',2,NULL,1,NULL,NULL,NULL),(62,NULL,NULL,0.98,'2014-03-19 00:33:36',2,NULL,1,NULL,NULL,NULL),(63,NULL,NULL,0.98,'2014-03-19 00:35:51',2,NULL,1,NULL,NULL,NULL),(64,NULL,NULL,0.98,'2014-03-19 00:37:45',2,NULL,1,NULL,NULL,NULL),(65,NULL,NULL,0.98,'2014-03-19 00:37:54',2,NULL,1,NULL,NULL,NULL),(66,NULL,NULL,0.98,'2014-03-19 00:38:29',2,NULL,1,NULL,NULL,NULL),(67,NULL,NULL,0.98,'2014-03-19 00:39:32',2,NULL,1,NULL,NULL,NULL),(68,NULL,NULL,0.98,'2014-03-19 00:39:39',2,NULL,1,NULL,NULL,NULL),(69,NULL,NULL,0.98,'2014-03-19 00:39:58',2,NULL,1,NULL,NULL,NULL),(70,NULL,NULL,0.00,'2014-03-19 00:43:42',2,NULL,1,NULL,NULL,NULL),(71,NULL,NULL,97.50,'2014-03-19 02:49:20',2,NULL,1,NULL,NULL,NULL),(72,NULL,NULL,101.40,'2014-03-21 14:28:50',2,NULL,1,NULL,NULL,NULL),(73,NULL,NULL,2.00,'2014-03-21 20:02:24',2,NULL,1,NULL,NULL,NULL),(74,NULL,NULL,2.00,'2014-03-21 20:03:28',2,NULL,1,NULL,NULL,NULL),(75,NULL,NULL,2.00,'2014-03-21 20:03:50',2,NULL,1,NULL,NULL,NULL),(76,NULL,NULL,2.00,'2014-03-21 20:08:33',2,NULL,1,NULL,NULL,NULL),(77,NULL,NULL,2.00,'2014-03-21 20:09:12',2,NULL,2,NULL,NULL,NULL),(78,NULL,NULL,24.00,'2014-03-22 10:35:13',2,NULL,2,NULL,NULL,NULL),(79,NULL,NULL,1183.00,'2014-03-22 14:38:40',2,NULL,1,NULL,NULL,NULL),(80,NULL,NULL,1183.00,'2014-03-22 14:39:04',2,NULL,1,NULL,NULL,NULL),(81,NULL,NULL,2.00,'2014-03-22 16:06:19',2,NULL,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `invoice_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_items`
--

DROP TABLE IF EXISTS `invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `expiery_date` date DEFAULT NULL,
  `purchase_price` decimal(10,2) DEFAULT NULL,
  `batch_no` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bonus` decimal(10,2) NOT NULL DEFAULT '0.00',
  `selling_price` decimal(10,2) NOT NULL,
  `discount1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount2` decimal(10,2) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table1_Invoice1_idx` (`invoice_id`),
  CONSTRAINT `fk_table1_Invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_items`
--

LOCK TABLES `invoice_items` WRITE;
/*!40000 ALTER TABLE `invoice_items` DISABLE KEYS */;
INSERT INTO `invoice_items` VALUES (28,52,2,'2009-09-09',NULL,'1',22.00,44.00,1.00,44.00,0.00,0.00,1.00,2.00),(29,52,2,'2009-09-09',NULL,'1',3.38,16.92,2.00,6.00,6.00,0.00,3.00,5.00),(30,57,3,'2014-03-19',NULL,'1',0.98,1.96,1.00,2.00,1.00,1.00,1.00,2.00),(31,58,1,'2009-09-09',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(32,59,2,'2009-09-09',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(33,60,2,'2009-09-09',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(34,61,2,'2009-09-09',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(35,62,2,'9009-01-01',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(36,63,1,'2001-01-01',NULL,'e',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(37,64,1,'2001-01-01',NULL,'e',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(38,65,1,'2001-01-01',NULL,'e',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(39,66,1,'2001-01-01',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(40,68,2,'2001-01-01',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(41,69,2,'2001-01-01',NULL,'1',0.49,0.98,1.00,1.00,1.00,1.00,1.00,2.00),(42,71,3,'2010-10-10',NULL,'12',15.00,60.00,1.00,25.00,16.00,4.00,3.00,4.00),(43,71,1,'2009-01-01',NULL,'1',6.25,37.50,1.00,25.00,70.00,0.00,5.00,6.00),(44,72,2,'2010-10-10',NULL,'123',1.28,6.40,1.00,2.00,15.00,5.00,4.00,5.00),(45,72,1,'2010-10-10',NULL,NULL,12.50,25.00,1.00,25.00,0.00,0.00,1.00,2.00),(46,72,2,'2010-10-10',NULL,NULL,1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(47,72,3,'2010-10-10',NULL,NULL,12.00,24.00,1.00,24.00,0.00,0.00,1.00,2.00),(48,72,2,'2010-10-10',NULL,NULL,1.91,44.00,1.00,2.00,0.00,0.00,22.00,23.00),(49,73,2,'2010-10-10',NULL,'1',1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(50,74,2,'2010-10-10',NULL,'1',1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(51,75,2,'2010-10-10',NULL,'1',1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(52,76,2,'2010-10-10',NULL,'1',1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(53,77,2,'2010-10-10',NULL,'1',1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00),(54,78,3,'2010-10-10',NULL,'1',12.00,24.00,1.00,24.00,0.00,0.00,1.00,2.00),(55,80,6,'2010-10-10',NULL,'12',295.75,1183.00,0.00,325.00,9.00,0.00,4.00,4.00),(56,81,2,'2015-10-12',NULL,NULL,1.00,2.00,1.00,2.00,0.00,0.00,1.00,2.00);
/*!40000 ALTER TABLE `invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'English','en'),(2,'Français','fr'),(3,'Deutsch','de'),(4,'Español','es'),(5,'Italiano','it'),(6,'Български','bg');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_header`
--

DROP TABLE IF EXISTS `order_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_price` decimal(10,2) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `discount_ammount` decimal(10,2) DEFAULT NULL,
  `customer_party_id` int(11) NOT NULL,
  `status` enum('requested','shiped','delivered') COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_header_user1_idx` (`user_id`),
  CONSTRAINT `fk_order_header_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_header`
--

LOCK TABLES `order_header` WRITE;
/*!40000 ALTER TABLE `order_header` DISABLE KEYS */;
INSERT INTO `order_header` VALUES (1,2,NULL,NULL,24.00,24.00,NULL,1,NULL,2),(2,2,NULL,NULL,24.00,24.00,NULL,1,NULL,2),(3,2,NULL,NULL,24.00,24.00,NULL,2,NULL,1),(4,2,'2014-03-22',NULL,24.00,24.00,NULL,2,NULL,1),(5,2,'2014-03-22',NULL,24.00,24.00,NULL,2,NULL,1),(6,2,'2014-03-22',NULL,24.00,24.00,NULL,2,NULL,1),(7,2,'2014-03-22',NULL,2.00,2.00,NULL,0,NULL,1);
/*!40000 ALTER TABLE `order_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_header_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `inventory_control_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_item_order_header1_idx` (`order_header_id`),
  KEY `fk_order_item_inventory_control1_idx` (`inventory_control_id`),
  CONSTRAINT `fk_order_item_inventory_control1` FOREIGN KEY (`inventory_control_id`) REFERENCES `inventory_control` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_item_order_header1` FOREIGN KEY (`order_header_id`) REFERENCES `order_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,6,2,3.00,31),(2,6,2,4.00,31),(3,6,2,5.00,31),(4,7,2,1.00,35);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `party`
--

DROP TABLE IF EXISTS `party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `party`
--

LOCK TABLES `party` WRITE;
/*!40000 ALTER TABLE `party` DISABLE KEYS */;
INSERT INTO `party` VALUES (1,'nagdy',NULL,NULL,''),(2,'alpha',NULL,NULL,'');
/*!40000 ALTER TABLE `party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `party_attr`
--

DROP TABLE IF EXISTS `party_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `party_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr_value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `party_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `party_attr`
--

LOCK TABLES `party_attr` WRITE;
/*!40000 ALTER TABLE `party_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `party_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `party_role`
--

DROP TABLE IF EXISTS `party_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `party_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Party_id` int(11) NOT NULL,
  `role` enum('supplier','customer') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `party_role`
--

LOCK TABLES `party_role` WRITE;
/*!40000 ALTER TABLE `party_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `party_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_allow` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_87209A8789329D25` (`resource_id`),
  KEY `IDX_87209A87D60322AC` (`role_id`),
  CONSTRAINT `FK_87209A8789329D25` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`),
  CONSTRAINT `FK_87209A87D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilege`
--

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arabic_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bar_code_1` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_barcode` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minimum_stock_amt` decimal(10,2) DEFAULT NULL,
  `default_customer_price` decimal(10,2) DEFAULT NULL,
  `default_purchase_price` decimal(10,2) DEFAULT NULL,
  `main_supplier_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'dell i3','لابتوب ديل اي 3 ',NULL,NULL,NULL,25.00,NULL,NULL,NULL),(2,'mouse pad','ماوس باد','1234','شسي',NULL,2.00,NULL,NULL,NULL),(3,'keyboard','كي بورد','key','9090',NULL,24.00,20.00,NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'screen 17\"',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attr`
--

DROP TABLE IF EXISTS `product_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attr_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr_value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attr`
--

LOCK TABLES `product_attr` WRITE;
/*!40000 ALTER TABLE `product_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_supplier`
--

DROP TABLE IF EXISTS `product_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_supplier` (
  `product_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_supplier`
--

LOCK TABLES `product_supplier` WRITE;
/*!40000 ALTER TABLE `product_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'guest'),(2,'member'),(3,'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_parents`
--

DROP TABLE IF EXISTS `roles_parents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_parents` (
  `role_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`parent_id`),
  KEY `IDX_C70E6B91D60322AC` (`role_id`),
  KEY `IDX_C70E6B91727ACA70` (`parent_id`),
  CONSTRAINT `FK_C70E6B91727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_C70E6B91D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_parents`
--

LOCK TABLES `roles_parents` WRITE;
/*!40000 ALTER TABLE `roles_parents` DISABLE KEYS */;
INSERT INTO `roles_parents` VALUES (2,1),(3,2);
/*!40000 ALTER TABLE `roles_parents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (1,'centrino1',NULL),(2,'centrino2',NULL);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_entry`
--

DROP TABLE IF EXISTS `transaction_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_entry` (
  `id` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_credit` enum('debit','credit') COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_transaction_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_entry_account_transaction1_idx` (`account_transaction_id`),
  CONSTRAINT `fk_transaction_entry_account_transaction1` FOREIGN KEY (`account_transaction_id`) REFERENCES `account_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_entry`
--

LOCK TABLES `transaction_entry` WRITE;
/*!40000 ALTER TABLE `transaction_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_salt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `registration_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8D93D649D60322AC` (`role_id`),
  KEY `IDX_8D93D64982F1BAF4` (`language_id`),
  CONSTRAINT `FK_8D93D64982F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`),
  CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,2,1,'ahmed.gamal@ahmedgamal.info','ahmed.gamal@ahmedgamal.info','-','-','c2f5d69e4f99ff9d3ca1c494829e0cd4','ahmed.gamal@ahmedgamal.info',1,NULL,NULL,NULL,'0pT\\$cKYNhfxckJ@M?E!s}+bFL7!W5tf\'ID+-ocZXKT=7}]c>$','2014-03-12 19:19:00','481dab98e4f2b5df992cee5b1de12d92',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-22 22:45:32
