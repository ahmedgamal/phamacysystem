<?php

return array (
		'router' => array (
				'routes' => array (
						// The following is a route to simplify getting started creating
						// new controllers and actions without needing to create a new
						// module. Simply drop new controllers in, and you can access them
						// using the path /application/:controller/:action
						'application' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/richstore',
										'defaults' => array (
												'__NAMESPACE__' => 'Richstore\Controller',
												'controller' => 'Index',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'default' => array (
												'type' => 'Segment',
												'options' => array (
														'route' => '/[:controller[/:action[/:id]]]',
														'constraints' => array (
																'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'id' => '[0-9]*' 
														)
														,
														'defaults' => array () 
												) 
										) 
								) 
						),
						'invalidLogin' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/invalidLogin',
										'defaults' => array (
												'__NAMESPACE__' => 'Richstore\Controller',
												'controller' => 'Index',
												'action' => 'invalidLogin'
										)
								),
						),
						'demo' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/demo',
										'defaults' => array (
												'__NAMESPACE__' => 'Application\Controller',
												'controller' => 'Index',
												'action' => 'demo'
										)
								),
						),
						
						
						'api' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/api',
										'defaults' => array (
												'__NAMESPACE__' => 'Richstore\Controller',
												'controller' => 'Index',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'default' => array (
												'type' => 'Segment',
												'options' => array (
														'route' => '/[:controller[/:action[:id]]]',
														'constraints' => array (
																'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'id' => '[0-9]*' 
														)
														,
														'defaults' => array (
																'__NAMESPACE__' => 'Richstore\Controller',
																//'controller' => 'Index',
																//'action' => 'index' 
														)
														 
												) 
										) 
								) 
						),
						'crud' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/crud',
										'defaults' => array (
												'__NAMESPACE__' => 'Richstore\Controller',
												//'controller' => 'Index',
												//'action' => 'index'
										)
								),
								'may_terminate' => true,
								'child_routes' => array (
										'default' => array (
												'type' => 'Segment',
												'options' => array (
														'route' => '/[:controller[/:id]]',
														'constraints' => array (
																'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
																//'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'id' => '[0-9]*'
														)
														,
														'defaults' => array (
																'__NAMESPACE__' => 'Richstore\Controller',
																//	'controller' => 'Index',
																//	'action' => 'index'
														)
															
												)
										)
								)
						),
						
				)
				 
		),
		'controllers' => array (
				'invokables' => array (
						'Richstore\Controller\Purchase' => 'Richstore\Controller\PurchaseController',
						'Richstore\Controller\Product' => 'Richstore\Controller\ProductController',
						'Richstore\Controller\Index' => 'Richstore\Controller\IndexController' ,
						'Richstore\Controller\Inventory' => 'Richstore\Controller\InventoryController' ,
						'Richstore\Controller\Order' => 'Richstore\Controller\OrderController' ,
						
				)
				 
		),
		'view_manager' => array (
				'display_not_found_reason' => true,
				'display_exceptions' => true,
				'doctype' => 'HTML5',
				'not_found_template' => 'error/404',
				'exception_template' => 'error/index',
				'template_map' => array (
						'layout/ng' => __DIR__ . '/../view/layout/ng.phtml' 
				)
				,
				'template_path_stack' => array (
						__DIR__ . '/../view' 
				),
				'strategies' => array (
						'ViewJsonStrategy' 
				) 
		),
		'doctrine' => array (
			'driver' => array (
				__NAMESPACE__ . '_driver' => array (
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array (
					__DIR__ . '/../src/Richstore/Entity' 
				) 
			),
			'orm_default' => array (
				'drivers' => array (
					'Richstore\Entity' => __NAMESPACE__ . '_driver' 
					) 
				) 
			) 
		) 
);
