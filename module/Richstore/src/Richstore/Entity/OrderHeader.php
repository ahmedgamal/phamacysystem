<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderHeader
 *
 * @ORM\Table(name="order_header", indexes={@ORM\Index(name="fk_order_header_user1_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class OrderHeader
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="date", nullable=true)
     */
    private $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=45, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="final_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $finalPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="total_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $totalPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_ammount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $discountAmmount;

    /**
     * @var \CsnUser\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Party")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_party_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \Richstore\Entity\Store
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var \CsnUser\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="CsnUser\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $createdBy;
    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="orderHeader")
     */
    public $orderItems;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return OrderHeader
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return OrderHeader
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set finalPrice
     *
     * @param string $finalPrice
     * @return OrderHeader
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;

        return $this;
    }

    /**
     * Get finalPrice
     *
     * @return string 
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     * @return OrderHeader
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string 
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set discountAmmount
     *
     * @param string $discountAmmount
     * @return OrderHeader
     */
    public function setDiscountAmmount($discountAmmount)
    {
        $this->discountAmmount = $discountAmmount;

        return $this;
    }

    /**
     * Get discountAmmount
     *
     * @return string 
     */
    public function getDiscountAmmount()
    {
        return $this->discountAmmount;
    }

    /**
     * Set customer
     *
     * @param Party $customer
     * @return OrderHeader
     */
    public function setCustomer($customer)
    {
    	$this->customer = $customer;
    
    	return $this;
    }
    
    /**
     * Get customer
     *
     * @return integer
     */
    public function getCustomer()
    {
    	return $this->customer;
    }

    /**
     * Set store
     *
     * @param Store $store
     * @return OrderHeader
     */
    public function setStore($store)
    {
    	$this->store = $store;
    
    	return $this;
    }
    
    /**
     * Get store
     *
     * @return integer
     */
    public function getStore()
    {
    	return $this->store;
    }
    
    /**
     * Set status
     *
     * @param string $status
     * @return OrderHeader
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdBy
     *
     * @param \CsnUser\Entity\User $createdBy
     * @return OrderHeader
     */
    public function setCreatedBy(\CsnUser\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CsnUser\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
