<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransactionEntry
 *
 * @ORM\Table(name="transaction_entry", indexes={@ORM\Index(name="fk_transaction_entry_account_transaction1_idx", columns={"account_transaction_id"})})
 * @ORM\Entity
 */
class TransactionEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=45, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="debit_credit", type="string", nullable=true)
     */
    private $debitCredit;

    /**
     * @var \Richstore\Entity\AccountTransaction
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\AccountTransaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_transaction_id", referencedColumnName="id")
     * })
     */
    private $accountTransaction;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return TransactionEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TransactionEntry
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set debitCredit
     *
     * @param string $debitCredit
     * @return TransactionEntry
     */
    public function setDebitCredit($debitCredit)
    {
        $this->debitCredit = $debitCredit;

        return $this;
    }

    /**
     * Get debitCredit
     *
     * @return string 
     */
    public function getDebitCredit()
    {
        return $this->debitCredit;
    }

    /**
     * Set accountTransaction
     *
     * @param \Richstore\Entity\AccountTransaction $accountTransaction
     * @return TransactionEntry
     */
    public function setAccountTransaction(\Richstore\Entity\AccountTransaction $accountTransaction = null)
    {
        $this->accountTransaction = $accountTransaction;

        return $this;
    }

    /**
     * Get accountTransaction
     *
     * @return \Richstore\Entity\AccountTransaction 
     */
    public function getAccountTransaction()
    {
        return $this->accountTransaction;
    }
}
