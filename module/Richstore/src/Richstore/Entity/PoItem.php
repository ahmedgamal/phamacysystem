<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoItem
 *
 * @ORM\Table(name="PO_item", indexes={@ORM\Index(name="fk_PO_items_PO_header1", columns={"PO_header_id"})})
 * @ORM\Entity
 */
class PoItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var \Richstore\Entity\PoHeader
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\PoHeader")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PO_header_id", referencedColumnName="id")
     * })
     */
    private $poHeader;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return PoItem
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PoItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set poHeader
     *
     * @param \Richstore\Entity\PoHeader $poHeader
     * @return PoItem
     */
    public function setPoHeader(\Richstore\Entity\PoHeader $poHeader = null)
    {
        $this->poHeader = $poHeader;

        return $this;
    }

    /**
     * Get poHeader
     *
     * @return \Richstore\Entity\PoHeader 
     */
    public function getPoHeader()
    {
        return $this->poHeader;
    }
}
