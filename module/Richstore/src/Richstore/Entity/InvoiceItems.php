<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceItems
 *
 * @ORM\Table(name="invoice_items", indexes={@ORM\Index(name="fk_table1_Invoice1_idx", columns={"invoice_id"})})
 * @ORM\Entity
 */
class InvoiceItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Richstore\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Product",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiery_date", type="date", nullable=true)
     */
    private $expieryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="purchase_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $purchasePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="batch_no", type="string", length=45, nullable=true)
     */
    public $batchNo;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_cost", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $unitCost;
    /**
     * @var string
     *
     * @ORM\Column(name="total_cost", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $totalCost;
    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=2, scale=2, nullable=true)
     */
    public $amount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="bonus", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $bonus;
    
    /**
     * @var string
     *
     * @ORM\Column(name="selling_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    public  $sellingPrice;
    
    /**
     * @var string
     *
     * @ORM\Column(name="discount1", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $discount1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="discount2", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $discount2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    public $totalAmount;
    

    /**
     * @var \Richstore\Entity\InvoiceHeader
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\InvoiceHeader")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $product
     * @return InvoiceItems
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set expieryDate
     *
     * @param \DateTime $expieryDate
     * @return InvoiceItems
     */
    public function setExpieryDate($expieryDate)
    {
        $this->expieryDate = $expieryDate;

        return $this;
    }

    /**
     * Get expieryDate
     *
     * @return \DateTime 
     */
    public function getExpieryDate()
    {
        return $this->expieryDate;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     * @return InvoiceItems
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string 
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set batchNo
     *
     * @param string $batchNo
     * @return InvoiceItems
     */
    public function setBatchNo($batchNo)
    {
        $this->batchNo = $batchNo;

        return $this;
    }

    /**
     * Get batchNo
     *
     * @return string 
     */
    public function getBatchNo()
    {
        return $this->batchNo;
    }

    /**
     * Set invoice
     *
     * @param \Richstore\Entity\InvoiceHeader $invoice
     * @return InvoiceItems
     */
    public function setInvoice(\Richstore\Entity\InvoiceHeader $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \Richstore\Entity\InvoiceHeader 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
