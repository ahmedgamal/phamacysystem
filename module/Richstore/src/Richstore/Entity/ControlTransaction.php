<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlTransaction
 *
 * @ORM\Table(name="control_transaction", indexes={@ORM\Index(name="fk_control_transaction_inventory_control1_idx", columns={"inventory_control_id"}), @ORM\Index(name="fk_control_transaction_user1_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class ControlTransaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=45, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=45, nullable=true)
     */
    private $orderId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="date", nullable=true)
     */
    private $dateCreated;

    /**
     * @var \Richstore\Entity\InventoryControl
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\InventoryControl")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inventory_control_id", referencedColumnName="id")
     * })
     */
    private $inventoryControl;

    /**
     * @var \CsnUser\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="CsnUser\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return ControlTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ControlTransaction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return ControlTransaction
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return ControlTransaction
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set inventoryControl
     *
     * @param \Richstore\Entity\InventoryControl $inventoryControl
     * @return ControlTransaction
     */
    public function setInventoryControl(\Richstore\Entity\InventoryControl $inventoryControl = null)
    {
        $this->inventoryControl = $inventoryControl;

        return $this;
    }

    /**
     * Get inventoryControl
     *
     * @return \Richstore\Entity\InventoryControl 
     */
    public function getInventoryControl()
    {
        return $this->inventoryControl;
    }

    /**
     * Set user
     *
     * @param \CsnUser\Entity\User $user
     * @return ControlTransaction
     */
    public function setUser(\CsnUser\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CsnUser\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
