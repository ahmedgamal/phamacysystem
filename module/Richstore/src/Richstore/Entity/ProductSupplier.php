<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductSupplier
 *
 * @ORM\Table(name="product_supplier")
 * @ORM\Entity
 */
class ProductSupplier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="party_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $partyId;



    /**
     * Set productId
     *
     * @param integer $productId
     * @return ProductSupplier
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set partyId
     *
     * @param integer $partyId
     * @return ProductSupplier
     */
    public function setPartyId($partyId)
    {
        $this->partyId = $partyId;

        return $this;
    }

    /**
     * Get partyId
     *
     * @return integer 
     */
    public function getPartyId()
    {
        return $this->partyId;
    }
}
