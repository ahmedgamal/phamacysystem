<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartyRole
 *
 * @ORM\Table(name="party_role")
 * @ORM\Entity
 */
class PartyRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Party_id", type="integer", nullable=false)
     */
    private $partyId;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", nullable=true)
     */
    private $role;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partyId
     *
     * @param integer $partyId
     * @return PartyRole
     */
    public function setPartyId($partyId)
    {
        $this->partyId = $partyId;

        return $this;
    }

    /**
     * Get partyId
     *
     * @return integer 
     */
    public function getPartyId()
    {
        return $this->partyId;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return PartyRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
}
