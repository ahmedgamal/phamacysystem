<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="arabic_name", type="string", length=45, nullable=true)
     */
    private $arabicName;

    /**
     * @var string
     *
     * @ORM\Column(name="bar_code_1", type="string", length=45, nullable=true)
     */
    private $barCode1;

    /**
     * @var string
     *
     * @ORM\Column(name="international_barcode", type="string", length=45, nullable=true)
     */
    private $internationalBarcode;

    /**
     * @var string
     *
     * @ORM\Column(name="minimum_stock_amt", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $minimumStockAmt;

    /**
     * @var string
     *
     * @ORM\Column(name="default_customer_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $defaultCustomerPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="default_purchase_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $defaultPurchasePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="main_supplier_id", type="string", length=45, nullable=true)
     */
    private $mainSupplierId;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=45, nullable=true)
     */
    private $unit;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set arabicName
     *
     * @param string $arabicName
     * @return Product
     */
    public function setArabicName($arabicName)
    {
        $this->arabicName = $arabicName;

        return $this;
    }

    /**
     * Get arabicName
     *
     * @return string 
     */
    public function getArabicName()
    {
        return $this->arabicName;
    }

    /**
     * Set barCode1
     *
     * @param string $barCode1
     * @return Product
     */
    public function setBarCode1($barCode1)
    {
        $this->barCode1 = $barCode1;

        return $this;
    }

    /**
     * Get barCode1
     *
     * @return string 
     */
    public function getBarCode1()
    {
        return $this->barCode1;
    }

    /**
     * Set internationalBarcode
     *
     * @param string $internationalBarcode
     * @return Product
     */
    public function setInternationalBarcode($internationalBarcode)
    {
        $this->internationalBarcode = $internationalBarcode;

        return $this;
    }

    /**
     * Get internationalBarcode
     *
     * @return string 
     */
    public function getInternationalBarcode()
    {
        return $this->internationalBarcode;
    }

    /**
     * Set minimumStockAmt
     *
     * @param string $minimumStockAmt
     * @return Product
     */
    public function setMinimumStockAmt($minimumStockAmt)
    {
        $this->minimumStockAmt = $minimumStockAmt;

        return $this;
    }

    /**
     * Get minimumStockAmt
     *
     * @return string 
     */
    public function getMinimumStockAmt()
    {
        return $this->minimumStockAmt;
    }

    /**
     * Set defaultCustomerPrice
     *
     * @param string $defaultCustomerPrice
     * @return Product
     */
    public function setDefaultCustomerPrice($defaultCustomerPrice)
    {
        $this->defaultCustomerPrice = $defaultCustomerPrice;

        return $this;
    }

    /**
     * Get defaultCustomerPrice
     *
     * @return string 
     */
    public function getDefaultCustomerPrice()
    {
        return $this->defaultCustomerPrice;
    }

    /**
     * Set defaultPurchasePrice
     *
     * @param string $defaultPurchasePrice
     * @return Product
     */
    public function setDefaultPurchasePrice($defaultPurchasePrice)
    {
        $this->defaultPurchasePrice = $defaultPurchasePrice;

        return $this;
    }

    /**
     * Get defaultPurchasePrice
     *
     * @return string 
     */
    public function getDefaultPurchasePrice()
    {
        return $this->defaultPurchasePrice;
    }

    /**
     * Set mainSupplierId
     *
     * @param string $mainSupplierId
     * @return Product
     */
    public function setMainSupplierId($mainSupplierId)
    {
        $this->mainSupplierId = $mainSupplierId;

        return $this;
    }

    /**
     * Get mainSupplierId
     *
     * @return string 
     */
    public function getMainSupplierId()
    {
        return $this->mainSupplierId;
    }

    /**
     * Set unit
     *
     * @param string $unit
     * @return Product
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit()
    {
        return $this->unit;
    }
}
