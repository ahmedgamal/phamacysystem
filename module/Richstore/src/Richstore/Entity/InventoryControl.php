<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventoryControl
 *
 * @ORM\Table(name="inventory_control", indexes={@ORM\Index(name="fk_inventory_item_invoice_items1_idx", columns={"invoice_item_id"})})
 * @ORM\Entity
 */
class InventoryControl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="balance", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $balance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiry_date", type="date", nullable=true)
     */
    private $expiryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=45, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=45, nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="store_id", type="integer", nullable=false)
     */
    private $storeId;

    /**
     * @var \Richstore\Entity\InvoiceItems
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\InvoiceItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_item_id", referencedColumnName="id")
     * })
     */
    private $invoiceItem;

    /**
     * @var \Richstore\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Product",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    public $product;
    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set balance
     *
     * @param string $balance
     * @return InventoryControl
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return InventoryControl
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return InventoryControl
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return InventoryControl
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set storeId
     *
     * @param integer $storeId
     * @return InventoryControl
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }

    /**
     * Get storeId
     *
     * @return integer 
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Set invoiceItem
     *
     * @param \Richstore\Entity\InvoiceItems $invoiceItem
     * @return InventoryControl
     */
    public function setInvoiceItem(\Richstore\Entity\InvoiceItems $invoiceItem = null)
    {
    	$this->invoiceItem = $invoiceItem;
    
    	return $this;
    }
    
    /**
     * Get invoiceItem
     *
     * @return \Richstore\Entity\InvoiceItems
     */
    public function getInvoiceItem()
    {
    	return $this->invoiceItem;
    }

    /**
     * Set product
     *
     * @param \Richstore\Entity\Product product
     * @return product
     */
    public function setProduct(\Richstore\Entity\Product $product = null)
    {
    	$this->product = $product;
    
    	return $this;
    }
    
    /**
     * Get product
     *
     * @return \Richstore\Entity\Product
     */
    public function getProduct()
    {
    	return $this->product;
    }
}
