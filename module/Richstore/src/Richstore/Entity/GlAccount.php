<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlAccount
 *
 * @ORM\Table(name="gl_account", indexes={@ORM\Index(name="fk_gl_account_gl_account1_idx", columns={"parent_gl_account_id"})})
 * @ORM\Entity
 */
class GlAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=45, nullable=true)
     */
    private $accountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="account_name", type="string", length=45, nullable=true)
     */
    private $accountName;

    /**
     * @var string
     *
     * @ORM\Column(name="account_description", type="string", length=45, nullable=true)
     */
    private $accountDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="balance", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $balance;

    /**
     * @var \Richstore\Entity\GlAccount
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\GlAccount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_gl_account_id", referencedColumnName="id")
     * })
     */
    private $parentGlAccount;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return GlAccount
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     * @return GlAccount
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string 
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set accountName
     *
     * @param string $accountName
     * @return GlAccount
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;

        return $this;
    }

    /**
     * Get accountName
     *
     * @return string 
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Set accountDescription
     *
     * @param string $accountDescription
     * @return GlAccount
     */
    public function setAccountDescription($accountDescription)
    {
        $this->accountDescription = $accountDescription;

        return $this;
    }

    /**
     * Get accountDescription
     *
     * @return string 
     */
    public function getAccountDescription()
    {
        return $this->accountDescription;
    }

    /**
     * Set balance
     *
     * @param string $balance
     * @return GlAccount
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set parentGlAccount
     *
     * @param \Richstore\Entity\GlAccount $parentGlAccount
     * @return GlAccount
     */
    public function setParentGlAccount(\Richstore\Entity\GlAccount $parentGlAccount = null)
    {
        $this->parentGlAccount = $parentGlAccount;

        return $this;
    }

    /**
     * Get parentGlAccount
     *
     * @return \Richstore\Entity\GlAccount 
     */
    public function getParentGlAccount()
    {
        return $this->parentGlAccount;
    }
}
