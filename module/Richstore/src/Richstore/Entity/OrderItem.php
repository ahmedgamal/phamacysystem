<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItem
 *
 * @ORM\Table(name="order_item", indexes={@ORM\Index(name="fk_order_item_order_header1_idx", columns={"order_header_id"}), @ORM\Index(name="fk_order_item_inventory_control1_idx", columns={"inventory_control_id"})})
 * @ORM\Entity
 */
class OrderItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

 
    /**
     * @var \Richstore\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var \Richstore\Entity\OrderHeader
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\OrderHeader")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_header_id", referencedColumnName="id")
     * })
     */
    private $orderHeader;

    /**
     * @var \Richstore\Entity\InventoryControl
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\InventoryControl")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inventory_control_id", referencedColumnName="id")
     * })
     */
    private $inventoryControl;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param integer $product
     * @return OrderItem
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return OrderItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set orderHeader
     *
     * @param \Richstore\Entity\OrderHeader $orderHeader
     * @return OrderItem
     */
    public function setOrderHeader(\Richstore\Entity\OrderHeader $orderHeader = null)
    {
        $this->orderHeader = $orderHeader;

        return $this;
    }

    /**
     * Get orderHeader
     *
     * @return \Richstore\Entity\OrderHeader 
     */
    public function getOrderHeader()
    {
        return $this->orderHeader;
    }

    /**
     * Set inventoryControl
     *
     * @param \Richstore\Entity\InventoryControl $inventoryControl
     * @return OrderItem
     */
    public function setInventoryControl(\Richstore\Entity\InventoryControl $inventoryControl = null)
    {
        $this->inventoryControl = $inventoryControl;

        return $this;
    }

    /**
     * Get inventoryControl
     *
     * @return \Richstore\Entity\InventoryControl 
     */
    public function getInventoryControl()
    {
        return $this->inventoryControl;
    }
}
