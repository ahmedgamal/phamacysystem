<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductAttr
 *
 * @ORM\Table(name="product_attr")
 * @ORM\Entity
 */
class ProductAttr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_name", type="string", length=45, nullable=true)
     */
    private $attrName;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_value", type="string", length=45, nullable=true)
     */
    private $attrValue;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return ProductAttr
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set attrName
     *
     * @param string $attrName
     * @return ProductAttr
     */
    public function setAttrName($attrName)
    {
        $this->attrName = $attrName;

        return $this;
    }

    /**
     * Get attrName
     *
     * @return string 
     */
    public function getAttrName()
    {
        return $this->attrName;
    }

    /**
     * Set attrValue
     *
     * @param string $attrValue
     * @return ProductAttr
     */
    public function setAttrValue($attrValue)
    {
        $this->attrValue = $attrValue;

        return $this;
    }

    /**
     * Get attrValue
     *
     * @return string 
     */
    public function getAttrValue()
    {
        return $this->attrValue;
    }
}
