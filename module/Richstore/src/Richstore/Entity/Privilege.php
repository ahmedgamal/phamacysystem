<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Privilege
 *
 * @ORM\Table(name="privilege", indexes={@ORM\Index(name="IDX_87209A8789329D25", columns={"resource_id"}), @ORM\Index(name="IDX_87209A87D60322AC", columns={"role_id"})})
 * @ORM\Entity
 */
class Privilege
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permission_allow", type="boolean", nullable=false)
     */
    private $permissionAllow;

    /**
     * @var \Richstore\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var \Richstore\Entity\Resource
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Resource")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     * })
     */
    private $resource;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Privilege
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set permissionAllow
     *
     * @param boolean $permissionAllow
     * @return Privilege
     */
    public function setPermissionAllow($permissionAllow)
    {
        $this->permissionAllow = $permissionAllow;

        return $this;
    }

    /**
     * Get permissionAllow
     *
     * @return boolean 
     */
    public function getPermissionAllow()
    {
        return $this->permissionAllow;
    }

    /**
     * Set role
     *
     * @param \Richstore\Entity\Role $role
     * @return Privilege
     */
    public function setRole(\Richstore\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Richstore\Entity\Role 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set resource
     *
     * @param \Richstore\Entity\Resource $resource
     * @return Privilege
     */
    public function setResource(\Richstore\Entity\Resource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \Richstore\Entity\Resource 
     */
    public function getResource()
    {
        return $this->resource;
    }
}
