<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartyAttr
 *
 * @ORM\Table(name="party_attr")
 * @ORM\Entity
 */
class PartyAttr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="party_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $partyId;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_name", type="string", length=45, nullable=true)
     */
    private $attrName;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_value", type="string", length=45, nullable=true)
     */
    private $attrValue;



    /**
     * Set id
     *
     * @param integer $id
     * @return PartyAttr
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partyId
     *
     * @param integer $partyId
     * @return PartyAttr
     */
    public function setPartyId($partyId)
    {
        $this->partyId = $partyId;

        return $this;
    }

    /**
     * Get partyId
     *
     * @return integer 
     */
    public function getPartyId()
    {
        return $this->partyId;
    }

    /**
     * Set attrName
     *
     * @param string $attrName
     * @return PartyAttr
     */
    public function setAttrName($attrName)
    {
        $this->attrName = $attrName;

        return $this;
    }

    /**
     * Get attrName
     *
     * @return string 
     */
    public function getAttrName()
    {
        return $this->attrName;
    }

    /**
     * Set attrValue
     *
     * @param string $attrValue
     * @return PartyAttr
     */
    public function setAttrValue($attrValue)
    {
        $this->attrValue = $attrValue;

        return $this;
    }

    /**
     * Get attrValue
     *
     * @return string 
     */
    public function getAttrValue()
    {
        return $this->attrValue;
    }
}
