<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceHeader
 *
 * @ORM\Table(name="invoice_header", indexes={@ORM\Index(name="fk_Invoice_PO_header1_idx", columns={"PO_header_id"}), @ORM\Index(name="fk_Invoice_user1_idx", columns={"created_by"})})
 * @ORM\Entity
 */
class InvoiceHeader
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_headercol", type="string", length=45, nullable=true)
     */
    private $invoiceHeadercol;

    /**
     * @var string
     *
     * @ORM\Column(name="total_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $totalPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", nullable=true)
     */
    private $paymentMethod;

    /**
     * @var \Richstore\Entity\Party
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\Party")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     * })
     */
    private $supplier;

    /**
     * @var string
     *
     * @ORM\Column(name="items_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $itemsPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $taxPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var \Richstore\Entity\PoHeader
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\PoHeader")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PO_header_id", referencedColumnName="id")
     * })
     */
    private $poHeader;

    /**
     * @var \CsnUser\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="CsnUser\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoiceHeadercol
     *
     * @param string $invoiceHeadercol
     * @return InvoiceHeader
     */
    public function setInvoiceHeadercol($invoiceHeadercol)
    {
        $this->invoiceHeadercol = $invoiceHeadercol;

        return $this;
    }

    /**
     * Get invoiceHeadercol
     *
     * @return string 
     */
    public function getInvoiceHeadercol()
    {
        return $this->invoiceHeadercol;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     * @return InvoiceHeader
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string 
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return InvoiceHeader
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     * @return InvoiceHeader
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set supplier
     *
     * @param Richstore\Entity\Party $supplier
     * @return InvoiceHeader
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return Richstore\Entity\Party 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set itemsPrice
     *
     * @param string $itemsPrice
     * @return InvoiceHeader
     */
    public function setItemsPrice($itemsPrice)
    {
        $this->itemsPrice = $itemsPrice;

        return $this;
    }

    /**
     * Get itemsPrice
     *
     * @return string 
     */
    public function getItemsPrice()
    {
        return $this->itemsPrice;
    }

    /**
     * Set taxPrice
     *
     * @param string $taxPrice
     * @return InvoiceHeader
     */
    public function setTaxPrice($taxPrice)
    {
        $this->taxPrice = $taxPrice;

        return $this;
    }

    /**
     * Get taxPrice
     *
     * @return string 
     */
    public function getTaxPrice()
    {
        return $this->taxPrice;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return InvoiceHeader
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set poHeader
     *
     * @param \Richstore\Entity\PoHeader $poHeader
     * @return InvoiceHeader
     */
    public function setPoHeader(\Richstore\Entity\PoHeader $poHeader = null)
    {
        $this->poHeader = $poHeader;

        return $this;
    }

    /**
     * Get poHeader
     *
     * @return \Richstore\Entity\PoHeader 
     */
    public function getPoHeader()
    {
        return $this->poHeader;
    }

    /**
     * Set createdBy
     *
     * @param \CsnUser\Entity\User $createdBy
     * @return InvoiceHeader
     */
    public function setCreatedBy(\CsnUser\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CsnUser\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
