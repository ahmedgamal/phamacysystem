<?php

namespace Richstore\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlAttr
 *
 * @ORM\Table(name="control_attr", indexes={@ORM\Index(name="fk_table1_inventory_control1_idx", columns={"inventory_control_id"})})
 * @ORM\Entity
 */
class ControlAttr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_name", type="string", length=45, nullable=true)
     */
    private $attrName;

    /**
     * @var string
     *
     * @ORM\Column(name="attr_value", type="string", length=45, nullable=true)
     */
    private $attrValue;

    /**
     * @var \Richstore\Entity\InventoryControl
     *
     * @ORM\ManyToOne(targetEntity="Richstore\Entity\InventoryControl")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inventory_control_id", referencedColumnName="id")
     * })
     */
    private $inventoryControl;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attrName
     *
     * @param string $attrName
     * @return ControlAttr
     */
    public function setAttrName($attrName)
    {
        $this->attrName = $attrName;

        return $this;
    }

    /**
     * Get attrName
     *
     * @return string 
     */
    public function getAttrName()
    {
        return $this->attrName;
    }

    /**
     * Set attrValue
     *
     * @param string $attrValue
     * @return ControlAttr
     */
    public function setAttrValue($attrValue)
    {
        $this->attrValue = $attrValue;

        return $this;
    }

    /**
     * Get attrValue
     *
     * @return string 
     */
    public function getAttrValue()
    {
        return $this->attrValue;
    }

    /**
     * Set inventoryControl
     *
     * @param \Richstore\Entity\InventoryControl $inventoryControl
     * @return ControlAttr
     */
    public function setInventoryControl(\Richstore\Entity\InventoryControl $inventoryControl = null)
    {
        $this->inventoryControl = $inventoryControl;

        return $this;
    }

    /**
     * Get inventoryControl
     *
     * @return \Richstore\Entity\InventoryControl 
     */
    public function getInventoryControl()
    {
        return $this->inventoryControl;
    }
}
