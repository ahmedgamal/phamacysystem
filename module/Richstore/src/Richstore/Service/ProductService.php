<?php

namespace Richstore\Controller;

use Richstore\Entity\Product;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\Controller\AbstractActionController;


use Zend\View\Model\JsonModel;

class ProductController extends AbstractRestfulController {
//class ProductController extends AbstractActionController {
    public function getList() {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $results= $em->createQuery('select p from Richstore\Entity\Product p ')->getArrayResult();

       
        return new JsonModel(array(
            'data' => $results)
        );
    }

    public function getAction() {

        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $p = $em->find('Richstore\Entity\Product', $id);
        
        
        //print_r($results);
        
        return new JsonModel($p);
    }
    public function searchAction() {
 
    	$em = $this
    	->getServiceLocator()
    	->get('Doctrine\ORM\EntityManager');
    
    
 
    //echo $this->params()->fromPost('searchText'); 
    $data = $this->processBodyContent($this->getRequest());
    //print_r($data);
   // die();
    	$query = $em->createQuery("select p from Richstore\Entity\Product p where p.name like :searchName or p.barCode1=:searchBarcode ");
    	$query->setParameter('searchName','%'. $data['searchText'].'%');
    	$query->setParameter('searchBarcode', $data['searchText'] );
    	 
     
    	$results = $query->getArrayResult();
    	//print_r($results );die();
    	
    	
    
    	return new JsonModel($results );
    }
    
    public function create($data) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $album = new Album();
        $album->setArtist($data['artist']);
        $album->setTitle($data['title']);

        $em->persist($album);
        $em->flush();

        return new JsonModel(array(
            'data' => $album->getId(),
        ));
    }

    public function update($id, $data) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $album = $em->find('Album\Model\Album', $id);
        $album->setArtist($data['artist']);
        $album->setTitle($data['title']);

        $album = $em->merge($album);
        $em->flush();

        return new JsonModel(array(
            'data' => $album->getId(),
        ));
    }

    public function delete($id) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $album = $em->find('Album\Model\Album', $id);
        $em->remove($album);
        $em->flush();

        return new JsonModel(array(
            'data' => 'deleted',
        ));
    }

}
