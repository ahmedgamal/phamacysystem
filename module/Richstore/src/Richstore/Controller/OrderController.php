<?php

namespace Richstore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
use Richstore\Entity\InvoiceHeader;
use Richstore\Entity\Invoice;
use Richstore\Entity\InvoiceItems;
use Richstore\Entity\InventoryControl;
use Richstore\Entity\ControlTransaction;
use Zend\View\Model\JsonModel;
use Richstore\Entity\OrderHeader;
use DoctrineORMModuleTest\Assets\Entity\Date;
use Richstore\Entity\OrderItem;


class OrderController extends AbstractRestfulController
{

   
    public function create($data){
    	$em = $this	->getServiceLocator() 	->get('Doctrine\ORM\EntityManager');
 
    	$order = new OrderHeader();
    	
    	$order ->setCreatedBy($this->identity());
    	$order ->setDateCreated(new \DateTime());
    	 
    	$order->setCustomer($em->getReference("Richstore\Entity\Party",$data['customerId']));
    	$order->setTotalPrice($data['totalPrice']);
    	$order->setFinalPrice($data['finalPrice']);
    	$order->setStore($em->getReference("Richstore\Entity\Store",$data['storeId']));
    	 
    	 
    	 
    	$em->persist($order);
    	$em->flush();

    
    	foreach ($data['orderItems'] as $item){
    		$inventoryControl = $em->getReference('Richstore\Entity\InventoryControl',$item['id']);
    		$orderItem = new OrderItem();
    		$orderItem->setAmount($item['amount']);
    		$orderItem->setInventoryControl($inventoryControl);
			$orderItem->setOrderHeader($order);
			$orderItem->setProduct($em->getReference('Richstore\Entity\Product',$item['product']['id']));
			$em->persist($orderItem);
			$em->flush();
				
			$inventoryControl->setBalance($inventoryControl->getBalance()-$item['amount']);
			$em->merge($inventoryControl);
			$em->flush();
    		

    		$transaction = new ControlTransaction();
    		$transaction->setInventoryControl($inventoryControl);
    		$transaction->setAmount($item['amount']*-1);
    		$transaction->setDateCreated(new \DateTime( ));
    		$transaction->setUser($this->identity());
    		$transaction->setDescription("Order Invoice No".$order->getId());
    		$em->persist($transaction);
    		$em->flush();
    		
    		//to add financil accountind recored here 
    		// to petty cash (of the store)
    			//from invetory (od the store)
    		//if cridet
    			//to vendor account 
    				//from ivnvetory of the store   
    	}
		return new JsonModel((array('type'=>'success','msg'=>'Order Invoice Added with Id '.$order->getId())));
    	
    }

    public function get($id) {

        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

       // $orders =>  $em->getRepository('Richstore\Entity\OrderHeader')->findAll(); 

       $orders= $em->createQuery('select o,oi,p,ic from Richstore\Entity\OrderHeader o join o.orderItems oi join oi.product p  join oi.inventoryControl ic where o.id=:id')
                ->setParameter("id", $id)
                ->getArrayResult();
 
        //print_r($results);
        
        return new JsonModel($orders[0]);
    }
    
    public function getList() {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

       // $orders =>  $em->getRepository('Richstore\Entity\OrderHeader')->findAll(); 

       $orders= $em->createQuery('select o,oi from Richstore\Entity\OrderHeader o join o.orderItems oi  ')
                ->getArrayResult();
 
        //print_r($results);
        
        return new JsonModel($orders);
    }
    
    
}

