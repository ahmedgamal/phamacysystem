<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Richstore\Controller;

 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 

class IndexController extends AbstractActionController
{
    public function indexAction()
    {     
    	
    	if($this->identity()){
	    	$user = array(	
	    			'userId'=>$this->identity()->getId(),
	    			'userRole'=>$this->identity()->getRole()->getId(),
	    	);
    	}else{
    		$user = array(
    				'userId'=>'guest',
    				'userRole'=>1,
    		);
    		
    	}
    	
     	$result = new  ViewModel(array(
    			'user' =>$user,
    		 
    			
    	));
    	
    	 return $result;
    	//return new ViewModel();
    }

    public function invalidLoginAction()
    {
    	$response = $this->getResponse();
    	$response->setStatusCode(401);
    
    }    

}
