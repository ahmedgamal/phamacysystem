<?php

namespace Richstore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
use Richstore\Entity\InvoiceHeader;
use Richstore\Entity\Invoice;
use Richstore\Entity\InvoiceItems;
use Richstore\Entity\InventoryControl;
use Richstore\Entity\ControlTransaction;
use Zend\View\Model\JsonModel;


class InventoryController extends AbstractRestfulController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    public function addAction()
    {
    	echo "ccc";die();
        return new ViewModel();
    }
    public function getList() {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
		
        $dql = 'select c,ii,p,i,s supplierName from Richstore\Entity\InventoryControl c left join c.invoiceItem ii  join c.product p join ii.invoice i join i.supplier s where 1=1 ';

        if(!$this->params()->fromQuery('showEmptyStock') || $this->params()->fromQuery('showEmptyStock')=='false'){
        	$dql.= 'and c.balance > 0.0 ';
        }

        if ($this->params()->fromQuery('supplierId')){
        	$dql=$dql. " and s.id=".$this->params()->fromQuery('supplierId');
        }
        
    	if ($this->params()->fromQuery('expiryDatebefore')){
        	$dql=$dql. " and c.expiryDate < '".$this->params()->fromQuery('expiryDatebefore')."'";
        }
        if ($this->params()->fromQuery('searchText')){
        	$dql=$dql. " and (p.name like '".$this->params()->fromQuery('searchText')."%' ";
        	$dql=$dql. "  or p.barCode1 =  '".$this->params()->fromQuery('searchText')."' ";
        	 
        	$dql=$dql. " )";
        	 
        }
        
        $query = $em->createQuery($dql );

        $results= $query->getArrayResult();
         
        //TODO add product to control 
        //TODO find how to add field in doctirnr(try create many to one and set id manually ) 
        //$results= $em->createQuery('select a, u from Album\Model\Album a join a.artists u')->getArrayResult();
        
       
        return new JsonModel( 
              $results)
         ;
    }
    public function lowStockAction() {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
		//where c.balance<p.minimumStockAmt 
        $dql = 'select sum(c.balance) bal,p.id, p.name,p.minimumStockAmt from Richstore\Entity\InventoryControl c left    join c.product p group by p.id   having bal < p.minimumStockAmt   ';
         $results= $em->createQuery($dql)
                ->getArrayResult();
         return new JsonModel(  $results  );
         ;
    }

    public function get($id) {
        $em = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $inventoryControl = $em->find('Richstore\Entity\InventoryControl ', 1);
        
        /*$results= $em->createQuery('select a, u, s from Album\Model\Album a join a.artists u join a.songs s where a.id=:id')
                ->setParameter("id", $id)
                ->getArrayResult();
*/
        //print_r($results);
        
        return new JsonModel($inventoryControl);
    }


    
}

