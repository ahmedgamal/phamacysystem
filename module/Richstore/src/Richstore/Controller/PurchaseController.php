<?php

namespace Richstore\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
use Richstore\Entity\InvoiceHeader;
use Richstore\Entity\Invoice;
use Richstore\Entity\InvoiceItems;
use Richstore\Entity\InventoryControl;
use Richstore\Entity\ControlTransaction;
use Zend\View\Model\JsonModel;
use Richstore\Entity\Product;


class PurchaseController extends AbstractRestfulController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    public function addAction()
    {
    	echo "ccc";die();
        return new ViewModel();
    }
    public function create($data){
    	$em = $this
    	->getServiceLocator()
    	->get('Doctrine\ORM\EntityManager');
    	//print_r($data);
 
    	$invoice = new InvoiceHeader();
    	$invoice->setCreatedBy($this->identity());
    	$invoice->setSupplier($em->getReference("Richstore\Entity\Party",$data['supplierId']));
    	$invoice->setTotalPrice($data['totalPrice']);
    	 
    	$em->persist($invoice);
    	$em->flush();
    	
    	$control = new InventoryControl();
    	
    	
    	foreach ($data['purchaseItems'] as $purItem){
    		
    		$invoiceItem = new InvoiceItems();
    		$productRef = $em->getReference('Richstore\Entity\Product',$purItem['id']);
    		
    		/*  [id] => 2
 
*/
			
    		$invoiceItem->setProduct($productRef) ;
    		$invoiceItem->setExpieryDate( new \DateTime($purItem['expiryDate'])) ;
    		$invoiceItem->setInvoice($invoice);
    		$invoiceItem->batchNo = $purItem['batchNo'];
    		$invoiceItem->amount = $purItem['amount'];
    		$invoiceItem->discount1=$purItem['discount'];
    		$invoiceItem->discount2=($purItem['discount2']);
    		$invoiceItem->bonus=($purItem['bonus']);
    		$invoiceItem->totalAmount=($purItem['totalAmount']);
    		
    		$invoiceItem->totalCost=($purItem['totalCost']);
    		$invoiceItem->sellingPrice = $purItem['defaultCustomerPrice'];
    		$invoiceItem->unitCost=($purItem['unitCost']);

    		$em->persist($invoiceItem);
    		$em->flush();
    		$control = new InventoryControl();
    		$control->setInvoiceItem($invoiceItem);
    		$control->setBalance($invoiceItem->totalAmount);
    		$control->setStoreId($data['storeId']);
    		$control->setExpiryDate( new \DateTime($purItem['expiryDate'])) ;
    		
    		//$p->id = $purItem['id'];
    		$control->setProduct($productRef);
    		
    		$em->persist($control);
    		$em->flush(); 

    		$transaction = new ControlTransaction();
    		$transaction->setInventoryControl($control);
    		$transaction->setAmount($invoiceItem->totalAmount);
    		$transaction->setDateCreated(new \DateTime( ));
    		$transaction->setUser($this->identity());
    		$transaction->setDescription("Purchase Invoice No".$invoice->getId());
    		$em->persist($transaction);
    		$em->flush();
    		
    		//to add financil accountind recored here 
    		// to petty cash (of the store)
    			//from invetory (od the store)
    		//if cridet
    			//to vendor account 
    				//from ivnvetory of the store   
    		
    	}
    	
    	
    	
		return new JsonModel((array('type'=>'success','msg'=>'newPurchaseInvoiceAdded with id '.$invoice->getId())));
    	
    }

}

